package main

import (
	"github.com/Sirupsen/logrus"
	"net/http"
	"io/ioutil"
)

func main() {
	http.HandleFunc("/", handle)
	logrus.Info("Echo server listening on port 80.")
	if err := http.ListenAndServe(":80", nil); err != nil {
		logrus.WithError(err).Error("ListenAndServe")
	}
}

func handle(w http.ResponseWriter, r *http.Request) {


	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logrus.WithError(err).Error("ReadAll Body")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	logrus.WithFields(logrus.Fields{
		"URL": r.URL.String(),
		"Method": r.Method,
		"Headers": r.Header,
		"Body": string(body),
		}).Info("Request")

	w.WriteHeader(http.StatusOK)
}